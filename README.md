DNS-PY
=========

DNS-PY is a simple DNS server that uses redis as it's host resolution DB.  If a requested alias is not found, the request is forwarded to an external DNS server.

Version
----

0.2.1

Tech
-----------

DNS-PY relies on several open source projects to run:

* [redis-server] - An advanced key-value store
* [redis-py] - Python client for Redis key-value store
* [python] - obviously

Installation
--------------

```sh
$ git clone git@bitbucket.org:beuford/dns-py.git
$ cd dns-py
$ sudo ./install
```

Usage
------------
```sh

Options:   
  -h, --help            show this help message and exit  
  -t TTL, --ttl=TTL     set server TTL  
  -v VERB, --verb=VERB  set logging verbosity (0 is lowest)  
  -u UPSTREAM, --upstream=UPSTREAM  
                        set UPSTREAM dns server (8.8.8.8)  
  -r REDIS, --redis=REDIS  
                        set REDIS server and port (localhost:6379)  
  -a, --auth            set as authoritative server  
  -l, --log             enable logging  
```
Populate the database:
```sh
# add domain to redis (with members per ip)
redis-cli SADD domain.com domain.com:01
redis-cli SADD domain.com domain.com:02

# give some ips to domain
redis-cli SET domain.com:01 127.0.0.1
redis-cli SET domain.com:02 127.0.0.2

# verify members
redis-cli SMEMBERS domain.com

# verify ip for member
redis-cli GET domain.com:01
```

To do
-----------
* Cache upstream requests, set timeout in redis according to ttl remaining

License
----

MIT

  [redis-server]: http://redis.io/
  [redis-py]: https://pypi.python.org/pypi/redis/
  [python]: http://www.python.org/getit/
    
